package com.chuangting.client.service.impl;

import com.chuangtang.client.domain.AppClientDO;
import com.chuangtang.client.query.AppClientQueryDO;
import com.chuangtang.client.service.IAppClientService;
import com.chuangting.client.dao.SysAppClientMapper;
import com.chuangting.client.entity.SysAppClient;
import com.chuangting.client.entity.SysAppClientExample;
import com.chuangting.common.result.ResultDO;
import com.chuangting.common.result.ResultSupport;
import com.chuangting.common.enums.ErrorCode;
import com.chuangting.common.util.BeanUtilsExtends;
import com.chuangting.common.util.EncryptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AppClientServiceImpl implements IAppClientService {

    @Autowired
    private SysAppClientMapper appClientMapper;

    @Override
    public ResultDO add(AppClientDO domain) {
        SysAppClient entity = new SysAppClient();
        ResultDO result = BeanUtilsExtends.copy(entity, domain);

        if (!result.isSuccess()) {
            result.setSuccess(false);
            return result;
        }

        entity.setSecret(EncryptionUtil.MD5Util.encodeMD5Hex(entity.getSecret()));

        int r = -1;

        try {
            r = appClientMapper.insertSelective(entity);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.SYSTEM_EXCEPTION);
            return result;
        }
        if (r < 0) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.ADD_APP_CLIENT_ERROR);
            return result;
        }

        return result;
    }

    @Override
    public ResultDO authentication(AppClientQueryDO queryDO) {

        ResultDO result = new ResultSupport();

        SysAppClientExample example = new SysAppClientExample();

        example.createCriteria().andAppNameEqualTo(queryDO.getAppName())
                .andSecretEqualTo(queryDO.getSecret()).andVersionEqualTo(queryDO.getVersion());

        List<SysAppClient> list = null;
        try {
            list = appClientMapper.selectByExample(example);
        } catch(Exception e) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.SYSTEM_EXCEPTION);
            return result;
        }

        if(list.size() == 0) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.AUTHENTICATION_APP_CLIENT_ERROR);
            return result;
        }

        String tekon = list.get(0).getAppName() + ":" + list.get(0).getSecret();
        try {
            tekon = EncryptionUtil.Base64Util.encode(new String(EncryptionUtil.DESUtil.encrypt(tekon.getBytes(), list.get(0).getSecret().getBytes())));
        } catch (Exception e) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.AUTHENTICATION_APP_CLIENT_ERROR);
            return result;
        }

        AppClientDO domain = getDomain(list.get(0));
        domain.setToken(tekon);

        result.setModel(ResultSupport.FIRST_MODEL_KEY, domain);
        return result;
    }

    private AppClientDO getDomain(SysAppClient entity) {
        if (entity == null) return null;
        AppClientDO domain = new AppClientDO();
        return BeanUtilsExtends.copyProperties(domain, entity) ? domain : null;
    }
}
