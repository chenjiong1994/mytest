package com.chuangtang.client.service;

import com.chuangtang.client.domain.AppClientDO;
import com.chuangtang.client.query.AppClientQueryDO;
import com.chuangting.common.result.ResultDO;

public interface IAppClientService {

    public ResultDO add(AppClientDO domain);

    public ResultDO authentication(AppClientQueryDO queryDO);
}
