package com.chuangtang.client.query;

import com.chuangting.common.domain.BaseQueryDO;

public class AppClientQueryDO  extends BaseQueryDO {

    private String appName;

    private String secret;

    private Integer version;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
