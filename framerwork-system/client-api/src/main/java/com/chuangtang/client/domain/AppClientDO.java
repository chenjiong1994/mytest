package com.chuangtang.client.domain;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "客端应用对象模型")
public class AppClientDO {

    @ApiModelProperty(value = "id", required = false)
    private Integer id;
    @ApiModelProperty(value = "应用名称", required = false)
    private String appName;
    @ApiModelProperty(value = "安全码", required = false)
    private String secret;
    @ApiModelProperty(value = "版本号", required = false)
    private Integer version;
    @ApiModelProperty(value = "产生的Token", required = false)
    private String token;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "AppClientDO{" +
                "id=" + id +
                ", appName='" + appName + '\'' +
                ", secret='" + secret + '\'' +
                ", version=" + version +
                ", token='" + token + '\'' +
                '}';
    }
}
