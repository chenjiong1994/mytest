package com.chuangting.common.result;


import com.chuangting.common.enums.ErrorCode;

import java.util.HashMap;
import java.util.Map;

public class ResultSupport implements ResultDO {

    private static final long serialVersionUID = 4661096805690919752L;

    private boolean success = true;

    private ErrorCode errorCode;


    private Map<String, Object> models = new HashMap<String, Object>(4);

    /**
     * 创建一个result。
     */
    public ResultSupport() {
    }

    /**
     * 创建一个result。
     *
     * @param success
     *            是否成功
     */
    public ResultSupport(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Object getModel(String key) {
        return getModels().get(key);
    }

    public void setModel(String key, Object model) {
        getModels().put(key, model);
    }

    public Map<String, Object> getModels() {
        return models;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }
}

