package com.chuangting.common.result;


import com.chuangting.common.enums.ErrorCode;

import java.io.Serializable;
import java.util.Map;


public interface ResultDO extends Serializable {

    String FIRST_MODEL_KEY = "first_model";

    String SECOND_MODEL_KEY = "second_model";

    String THIRD_MODEL_KEY = "third_model";


    /**
     * 设置请求成功标志。
     *
     * @param success
     *            成功标志
     */
    void setSuccess(boolean success);

    /**
     * 请求是否成功。
     *
     * @return 如果成功，则返回<code>true</code>
     */
    boolean isSuccess();

    /**
     * 取得model对象
     *
     * @param key
     *            字符串key
     * @return model对象
     */
    Object getModel(String key);

    /**
     * 设置model对象。
     *
     * @param key
     *            字符串key
     * @param model
     *            model对象
     */
    void setModel(String key, Object model);

    /**
     * 取得所有model对象。
     *
     * @return model对象表
     */
    Map<String, Object> getModels();

    public ErrorCode getErrorCode();

    public void setErrorCode(ErrorCode errorCode);
}
