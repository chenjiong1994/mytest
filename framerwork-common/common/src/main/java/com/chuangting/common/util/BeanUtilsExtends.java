package com.chuangting.common.util;


import com.chuangting.common.result.ResultSupport;
import org.apache.commons.beanutils.BeanUtils;
import com.chuangting.common.enums.ErrorCode;
/**
 * @author reagan
 * @Description:实体拷贝类
 */
public class BeanUtilsExtends {

    /**
     * 拷贝数据到DTO对象
     *
     * @param dest 目标DTO
     * @param orig 源DAO
     * @return 是否拷贝成功 true成功false失败
     */
    public static boolean copyProperties(Object dest, Object orig) {
        try {
            BeanUtils.copyProperties(dest, orig);
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    /* 本来就是个简单的调用还包装一个方法来这就是脱了裤子放屁
     * 拷贝DTO到实体对象
     */
    public static ResultSupport copy(Object dest, Object orig) {
        ResultSupport result = new ResultSupport();
        try {
            copyProperties(dest, orig);
        } catch (Exception e) {
            result.setErrorCode(ErrorCode.SYSTEM_COPY_OBJECT_ERROR);
            result.setSuccess(false);
        }
        return result;
    }

}





