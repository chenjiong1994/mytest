package com.chuangting.common.util;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayOutputStream;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class EncryptionUtil {

    static public class AESUtil {

        private static final String KEY_AES = "AES";

        public static String encrypt(String src, String key) throws Exception {
            if (key == null || key.length() != 16) {
                throw new Exception("key不满足条件");
            }
            byte[] raw = key.getBytes();
            SecretKeySpec skeySpec = new SecretKeySpec(raw, KEY_AES);
            Cipher cipher = Cipher.getInstance(KEY_AES);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            byte[] encrypted = cipher.doFinal(src.getBytes());
            return byte2hex(encrypted);
        }

        public static String decrypt(String src, String key) throws Exception {
            if (key == null || key.length() != 16) {
                throw new Exception("key不满足条件");
            }
            byte[] raw = key.getBytes();
            SecretKeySpec skeySpec = new SecretKeySpec(raw, KEY_AES);
            Cipher cipher = Cipher.getInstance(KEY_AES);
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            byte[] encrypted1 = hex2byte(src);
            byte[] original = cipher.doFinal(encrypted1);
            String originalString = new String(original);
            return originalString;
        }

        private static byte[] hex2byte(String strhex) {
            if (strhex == null) {
                return null;
            }
            int l = strhex.length();
            if (l % 2 == 1) {
                return null;
            }
            byte[] b = new byte[l / 2];
            for (int i = 0; i != l / 2; i++) {
                b[i] = (byte) Integer.parseInt(strhex.substring(i * 2, i * 2 + 2),
                        16);
            }
            return b;
        }

        private static String byte2hex(byte[] b) {
            String hs = "";
            String stmp = "";
            for (int n = 0; n < b.length; n++) {
                stmp = (java.lang.Integer.toHexString(b[n] & 0XFF));
                if (stmp.length() == 1) {
                    hs = hs + "0" + stmp;
                } else {
                    hs = hs + stmp;
                }
            }
            return hs.toUpperCase();
        }

    }

    static public class Base64Util {

        /**
         * 字符编码
         */
        public final static String ENCODING = "UTF-8";

        /**
         * Base64编码
         *
         * @param data 待编码数据
         * @return String 编码数据
         * @throws Exception
         */
        public static String encode(String data) throws Exception {

            // 执行编码
            byte[] b = Base64.encodeBase64(data.getBytes(ENCODING));

            return new String(b, ENCODING);
        }

        /**
         * Base64安全编码<br>
         * 遵循RFC 2045实现
         *
         * @param data 待编码数据
         * @return String 编码数据
         * @throws Exception
         */
        public static String encodeSafe(String data) throws Exception {

            // 执行编码
            byte[] b = Base64.encodeBase64(data.getBytes(ENCODING), true);

            return new String(b, ENCODING);
        }

        /**
         * Base64解码
         *
         * @param data 待解码数据
         * @return String 解码数据
         * @throws Exception
         */
        public static String decode(String data) throws Exception {

            // 执行解码
            byte[] b = Base64.decodeBase64(data.getBytes(ENCODING));

            return new String(b, ENCODING);
        }

    }

    static public class DESUtil {
        /**
         * 密钥算法 <br>
         * Java 6 只支持56bit密钥 <br>
         * Bouncy Castle 支持64bit密钥
         */
        public static final String KEY_ALGORITHM = "DES";
        /**
         * 加密/解密算法 / 工作模式 / 填充方式
         */
        public static final String CIPHER_ALGORITHM = "DES/ECB/PKCS5PADDING";

        static {
            Security.insertProviderAt(new BouncyCastleProvider(), 1);
        }

        /**
         * 转换密钥
         *
         * @param key 二进制密钥
         * @return Key 密钥
         * @throws Exception
         */
        private static Key toKey(byte[] key) throws Exception {

            // 实例化DES密钥材料
            DESKeySpec dks = new DESKeySpec(key);

            // 实例化秘密密钥工厂
            SecretKeyFactory keyFactory = SecretKeyFactory
                    .getInstance(KEY_ALGORITHM);

            // 生成秘密密钥
            SecretKey secretKey = keyFactory.generateSecret(dks);

            return secretKey;
        }

        /**
         * 解密
         *
         * @param data 待解密数据
         * @param key  密钥
         * @return byte[] 解密数据
         * @throws Exception
         */
        public static byte[] decrypt(byte[] data, byte[] key) throws Exception {

            // 还原密钥
            Key k = toKey(key);

            // 实例化
            Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);

            // 初始化，设置为解密模式
            cipher.init(Cipher.DECRYPT_MODE, k);

            // 执行操作
            return cipher.doFinal(data);
        }

        /**
         * 加密
         *
         * @param data 待加密数据
         * @param key  密钥
         * @return byte[] 加密数据
         * @throws Exception
         */
        public static byte[] encrypt(byte[] data, byte[] key) throws Exception {

            // 还原密钥
            Key k = toKey(key);

            // 实例化
            Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);

            // 初始化，设置为加密模式
            cipher.init(Cipher.ENCRYPT_MODE, k);

            // 执行操作
            return cipher.doFinal(data);
        }

        /**
         * 生成密钥 <br>
         * Java 6 只支持56bit密钥 <br>
         * Bouncy Castle 支持64bit密钥 <br>
         *
         * @return byte[] 二进制密钥
         * @throws Exception
         */
        public static byte[] initKey() throws Exception {

            /*
             * 实例化密钥生成器
             *
             * 若要使用64bit密钥注意替换 将下述代码中的KeyGenerator.getInstance(CIPHER_ALGORITHM);
             * 替换为KeyGenerator.getInstance(CIPHER_ALGORITHM, "BC");
             */
            KeyGenerator kg = KeyGenerator.getInstance(KEY_ALGORITHM);

            /*
             * 初始化密钥生成器 若要使用64bit密钥注意替换 将下述代码kg.init(56); 替换为kg.init(64);
             */
            kg.init(56, new SecureRandom());

            // 生成秘密密钥
            SecretKey secretKey = kg.generateKey();

            // 获得密钥的二进制编码形式
            return secretKey.getEncoded();
        }

        public static byte[] initKey(String seed) throws Exception {
            KeyGenerator kg = KeyGenerator.getInstance(KEY_ALGORITHM);
            SecureRandom secureRandom = new SecureRandom(new Base64().decode(seed));
            kg.init(secureRandom);
            SecretKey secretKey = kg.generateKey();
            return secretKey.getEncoded();
        }
    }

    static public class MD5Util {

        /**
         * MD5加密
         *
         * @param data 待加密数据
         * @return byte[] 消息摘要
         * @throws Exception
         */
        public static byte[] encodeMD5(String data) throws Exception {

            // 执行消息摘要
            return DigestUtils.md5(data);
        }

        /**
         * MD5加密
         *
         * @param data 待加密数据
         * @return byte[] 消息摘要
         * @throws Exception
         */
        public static String encodeMD5Hex(String data) {
            // 执行消息摘要
            return DigestUtils.md5Hex(data);
        }
    }

    static public class RSAUtil {
        /**
         * 非对称加密密钥算法
         */
        public static final String KEY_ALGORITHM_RSA = "RSA";

        /**
         * 公钥
         */
        private static final String RSA_PUBLIC_KEY = "RSAPublicKey";

        /**
         * 私钥
         */
        private static final String RSA_PRIVATE_KEY = "RSAPrivateKey";

        /**
         * RSA密钥长度
         * 默认1024位，
         * 密钥长度必须是64的倍数，
         * 范围在512至65536位之间。
         */
        private static final int KEY_SIZE = 1024;

        static {
            Security.insertProviderAt(new BouncyCastleProvider(), 1);
        }

        /**
         * 私钥解密
         *
         * @param data 待解密数据
         * @param key  私钥
         * @return byte[] 解密数据
         * @throws Exception
         */
        public static byte[] decryptByPrivateKey(byte[] data, byte[] key)
                throws Exception {

            // 取得私钥
            PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(key);

            KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM_RSA);

            // 生成私钥
            PrivateKey privateKey = keyFactory.generatePrivate(pkcs8KeySpec);

            // 对数据解密
            Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());

            cipher.init(Cipher.DECRYPT_MODE, privateKey);

            int blockSize = cipher.getBlockSize();
            if (blockSize > 0) {
                ByteArrayOutputStream bout = new ByteArrayOutputStream(64);
                int j = 0;
                while (data.length - j * blockSize > 0) {
                    bout.write(cipher.doFinal(data, j * blockSize, blockSize));
                    j++;
                }
                return bout.toByteArray();
            }
            return cipher.doFinal(data);
        }

        /**
         * 公钥解密
         *
         * @param data 待解密数据
         * @param key  公钥
         * @return byte[] 解密数据
         * @throws Exception
         */
        public static byte[] decryptByPublicKey(byte[] data, byte[] key)
                throws Exception {

            // 取得公钥
            X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(key);

            KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM_RSA);

            // 生成公钥
            PublicKey publicKey = keyFactory.generatePublic(x509KeySpec);

            // 对数据解密
            Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());

            cipher.init(Cipher.DECRYPT_MODE, publicKey);

            return cipher.doFinal(data);
        }

        /**
         * 公钥加密
         *
         * @param data 待加密数据
         * @param key  公钥
         * @return byte[] 加密数据
         * @throws Exception
         */
        public static byte[] encryptByPublicKey(byte[] data, byte[] key)
                throws Exception {

            // 取得公钥
            X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(key);

            KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM_RSA);

            PublicKey publicKey = keyFactory.generatePublic(x509KeySpec);

            // 对数据加密
            Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());

            cipher.init(Cipher.ENCRYPT_MODE, publicKey);

            int blockSize = cipher.getBlockSize();
            if (blockSize > 0) {
                int outputSize = cipher.getOutputSize(data.length);
                int leavedSize = data.length % blockSize;
                int blocksSize = leavedSize != 0 ? data.length / blockSize + 1
                        : data.length / blockSize;
                byte[] raw = new byte[outputSize * blocksSize];
                int i = 0, remainSize = 0;
                while ((remainSize = data.length - i * blockSize) > 0) {
                    int inputLen = remainSize > blockSize ? blockSize : remainSize;
                    cipher.doFinal(data, i * blockSize, inputLen, raw, i * outputSize);
                    i++;
                }
                return raw;
            }
            return cipher.doFinal(data);
        }

        /**
         * 私钥加密
         *
         * @param data 待加密数据
         * @param key  私钥
         * @return byte[] 加密数据
         * @throws Exception
         */
        public static byte[] encryptByPrivateKey(byte[] data, byte[] key)
                throws Exception {

            // 取得私钥
            PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(key);

            KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM_RSA);

            // 生成私钥
            PrivateKey privateKey = keyFactory.generatePrivate(pkcs8KeySpec);

            // 对数据加密
            Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());

            cipher.init(Cipher.ENCRYPT_MODE, privateKey);

            int blockSize = cipher.getBlockSize();
            if (blockSize > 0) {
                int outputSize = cipher.getOutputSize(data.length);
                int leavedSize = data.length % blockSize;
                int blocksSize = leavedSize != 0 ? data.length / blockSize + 1
                        : data.length / blockSize;
                byte[] raw = new byte[outputSize * blocksSize];
                int i = 0, remainSize = 0;
                while ((remainSize = data.length - i * blockSize) > 0) {
                    int inputLen = remainSize > blockSize ? blockSize : remainSize;
                    cipher.doFinal(data, i * blockSize, inputLen, raw, i * outputSize);
                    i++;
                }
                return raw;
            }
            return cipher.doFinal(data);
        }

        /**
         * 取得私钥
         *
         * @param keyMap 密钥Map
         * @return key 私钥
         * @throws Exception
         */
        public static Key getPrivateKey(Map<String, Key> keyMap)
                throws Exception {
            return keyMap.get(RSA_PRIVATE_KEY);
        }

        /**
         * 取得私钥
         *
         * @param keyMap 密钥Map
         * @return byte[] 私钥
         * @throws Exception
         */
        public static byte[] getPrivateKeyByte(Map<String, Key> keyMap)
                throws Exception {
            return keyMap.get(RSA_PRIVATE_KEY).getEncoded();
        }

        /**
         * 取得公钥
         *
         * @param keyMap 密钥Map
         * @return key 公钥
         * @throws Exception
         */
        public static Key getPublicKey(Map<String, Key> keyMap)
                throws Exception {
            return keyMap.get(RSA_PUBLIC_KEY);
        }

        /**
         * 取得公钥
         *
         * @param keyMap 密钥Map
         * @return byte[] 公钥
         * @throws Exception
         */
        public static byte[] getPublicKeyByte(Map<String, Key> keyMap)
                throws Exception {
            return keyMap.get(RSA_PUBLIC_KEY).getEncoded();
        }

        /**
         * 初始化密钥
         *
         * @param  seed 种子
         * @return Map 密钥Map
         * @throws Exception
         */
        public static Map<String, Key> initKey(byte[] seed) throws Exception {
            // 实例化密钥对生成器
            KeyPairGenerator keyPairGen = KeyPairGenerator
                    .getInstance(KEY_ALGORITHM_RSA);

            // 初始化密钥对生成器
            keyPairGen.initialize(KEY_SIZE, new SecureRandom(seed));

            // 生成密钥对
            KeyPair keyPair = keyPairGen.generateKeyPair();

            // 公钥
            RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();

            // 私钥
            RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();

            // 封装密钥
            Map<String, Key> keyMap = new HashMap<String, Key>(2);

            keyMap.put(RSA_PUBLIC_KEY, publicKey);
            keyMap.put(RSA_PRIVATE_KEY, privateKey);

            return keyMap;
        }

        /**
         * 初始化密钥
         *
         * @param seed 种子
         * @return Map 密钥Map
         * @throws Exception
         */
        public static Map<String, Key> initKey(String seed) throws Exception {
            return initKey(seed.getBytes());
        }

        /**
         * 初始化密钥
         *
         * @return Map 密钥Map
         * @throws Exception
         */
        public static Map<String, Key> initKey() throws Exception {
            return initKey(UUID.randomUUID().toString().getBytes());
        }

        public static PublicKey getPublicRSAKey(String key) throws Exception {
            X509EncodedKeySpec x509 = new X509EncodedKeySpec(org.bouncycastle.util.encoders.Base64.decode(key));
            KeyFactory kf = KeyFactory.getInstance(KEY_ALGORITHM_RSA);
            return kf.generatePublic(x509);
        }

        public static PrivateKey getPrivateRSAKey(String key) throws Exception {
            PKCS8EncodedKeySpec pkgs8 = new PKCS8EncodedKeySpec(org.bouncycastle.util.encoders.Base64.decode(key));
            KeyFactory kf = KeyFactory.getInstance(KEY_ALGORITHM_RSA);
            return kf.generatePrivate(pkgs8);
        }

    }

    public static void main(String[] args) throws Exception{
        String key = EncryptionUtil.MD5Util.encodeMD5Hex("anyoujia-app-client");
        System.out.println(key);
        System.out.println(EncryptionUtil.Base64Util.encode(new String(EncryptionUtil.DESUtil.encrypt("123456".getBytes(), key.getBytes()))));

    }
}
