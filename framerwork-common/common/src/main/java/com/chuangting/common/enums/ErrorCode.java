package com.chuangting.common.enums;

public enum ErrorCode {
    SYSTEM_EXCEPTION(500, "系统异常"),

    SYSTEM_COPY_OBJECT_ERROR(500, "复制对象错误"),

    ADD_ADMIN_ERROR(1001, "添加管理员失败"),

    EDIT_ADMIN_ERROR(1002, "修改管理员失败"),

    DELETE_ADMIN_ERROR(1003, "删除管理员失败"),

    GET_ADMIN_ERROR(1004, "获取管理员失败"),

    ADD_ROLE_ERROR(3001, "添加角色失败"),

    EDIT_ROLE_ERROR(3002, "修改角色失败"),

    DELETE_ROLE_ERROR(3003, "删除角色失败"),

    GET_ROLE_ERROR(3004, "获取角色失败"),

    ADD_PERMISSIONS_ERROR(4001, "添加权利失败"),

    EDIT_PERMISSIONS_ERROR(4002, "修改权利失败"),

    DELETE_PERMISSIONS_ERROR(4003, "删除权利失败"),

    GET_PERMISSIONS_ERROR(4004, "获取权利失败"),

    ADD_APP_CLIENT_ERROR(5001, "添加客户端程序失败"),

    AUTHENTICATION_APP_CLIENT_ERROR(5002, "验证客户端失败"),

    TEKON_APP_CLIENT_ERROR(5003, "客户端生成令牌错误"),

    ADD_ROLE_PERMISSIONS_ERROR(3005, "添加角色对应的权限失败"),

    DELETE_ROLE_PERMISSIONS_ERROR(3006, "添加角色对应的权限失败"),

    ROLE_PERMISSIONS_NULL(3007, "权限列表为空"),

    LOGIN_USERNAME_NULL(2002, "用户名为空"),

    LOGIN_PASSWORD_NULL(2003, "密码为空"),

    LOGIN_ERROR(2004, "管理员登陆失败");

    private int code;

    private String msg;

    private ErrorCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
