package com.chuangting.common.util;

import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

import java.util.HashMap;
import java.util.Map;

public class PasswordUtil {

    public static String SALE = "sale";

    public static String PASSWORD = "password";
    //随机数生成器
    private static RandomNumberGenerator randomNumberGenerator = new SecureRandomNumberGenerator();
    //指定散列算法为md5
    private static String algorithmName = "MD5";
    //散列迭代次数
    private static final int hashIterations = 1024;

    public static Map<String, String> encrypt(String password) {
        Map<String, String> values = new HashMap<>();
        String salt = randomNumberGenerator.nextBytes().toHex();
        String hashPwd = new SimpleHash(algorithmName,password, ByteSource.Util.bytes(salt), hashIterations).toHex();
        values.put(SALE, salt);
        values.put(PASSWORD, hashPwd);
        return values;
    }
}
