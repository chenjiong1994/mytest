package com.chuangting.common.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "基础请求对象模型")
public class JsonRequestDO {

    @ApiModelProperty(value = "客户端的token", required = false)
    public String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
