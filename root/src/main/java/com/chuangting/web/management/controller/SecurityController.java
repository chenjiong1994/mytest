package com.chuangting.web.management.controller;

import com.chuangting.admin.domain.AdminLoginDO;
import com.chuangting.common.util.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/")
public class SecurityController {

    @RequestMapping(value="/login.html", method = RequestMethod.GET)
    public ModelAndView login(ModelAndView mav, HttpServletRequest request) {
        mav.setViewName("/admin/login");
        return mav;
    }

    @RequestMapping(value="/login.html", method = RequestMethod.POST)
    public ModelAndView login(ModelAndView mav, AdminLoginDO domain, HttpServletRequest request) {
        String msg = null;

        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(domain.getUsername(), domain.getPassword());
        try {
            subject.login(token);
        } catch (UnknownAccountException e) {
            msg = "用户名";
        } catch (IncorrectCredentialsException e) {
            msg = "密码错误";
        } catch (AuthenticationException e) {
            //其他错误，比如锁定，如果想单独处理请单独catch处理
            msg = "验证失败";
        }
        if(StringUtils.isNotEmpty(msg)) {
            mav.addObject("MESSAGE", msg);
            mav.setViewName("/admin/login");
        } else {
           mav = new ModelAndView("redirect:/index.htm");
        }
        return mav;
    }

    @RequestMapping(value="/index.html", method = RequestMethod.GET)
    public ModelAndView index(ModelAndView mav, HttpServletRequest request) {
        return mav;
    }

    @RequestMapping(value="/403.html", method = RequestMethod.GET)
    public ModelAndView loginError(ModelAndView mav, HttpServletRequest request) {
        mav.setViewName("403");
        return mav;
    }
}
