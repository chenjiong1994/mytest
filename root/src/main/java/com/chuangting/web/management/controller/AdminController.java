package com.chuangting.web.management.controller;

import com.chuangting.admin.domain.AdminLoginDO;
import com.chuangting.admin.query.AdminQueryDO;
import com.chuangting.admin.service.IAdminService;
import com.chuangting.common.result.ResultDO;
import com.chuangting.common.result.ResultSupport;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Controller
@RequestMapping("/admin")
@Api(value = "AdminController | 管理员控制器")
public class AdminController {

    @Autowired
    private IAdminService adminService;

    @RequiresRoles(value={"admin"})
    @RequiresPermissions(value={"admin:add"})
    @RequestMapping(value="/add.html", method = RequestMethod.GET)
    public ModelAndView add(ModelAndView mav, HttpServletRequest request) {
        mav.setViewName("/admin/add");
        return mav;
    }

    @RequiresRoles(value={"admin"})
    @RequiresPermissions(value={"admin:add"})
    @RequestMapping(value="/add.html", method = RequestMethod.POST)
    @ApiOperation(value="添加用户", notes="添加管理员账号")
    @ApiImplicitParam(paramType="AdminLoginDO", name = "domain", value = "管理员DO对象", required = true)
    public ModelAndView add(ModelAndView mav, AdminLoginDO domain, HttpServletRequest request) {

        ResultDO result = adminService.add(domain);
        if(result.isSuccess()) {
            mav.setViewName("/success");
        } else {
            mav.addObject("ERROR_MSG", result.getErrorCode().getMsg());
            mav.setViewName("/error");
        }
        return mav;
    }


    @RequestMapping(value="/list.html", method = RequestMethod.GET)
    @ApiOperation(value="管理员用户列表", notes="管理员用户列表查询")
    @ApiImplicitParam(paramType="AdminQueryDO", name = "queryDO", value = "管理员查询DO对象", required = true)
    public ModelAndView list(ModelAndView mav, AdminQueryDO queryDO, HttpServletRequest request) {

        if(queryDO.getPageNum() != null && queryDO.getRows() != null) {
            PageHelper.startPage(queryDO.getPageNum(), queryDO.getRows());
        }
        ResultDO result = adminService.query(queryDO);
        if(result.isSuccess()) {
            PageInfo<AdminLoginDO> pageInfo = new PageInfo<AdminLoginDO>((List<AdminLoginDO>) result.getModel(ResultSupport.FIRST_MODEL_KEY));
            mav.addObject("PAGE_INFO", pageInfo);
            mav.setViewName("/admin/list");
        } else {
            mav.addObject("ERROR_MSG", result.getErrorCode().getMsg());
            mav.setViewName("/error");
        }

        return mav;
    }
}
