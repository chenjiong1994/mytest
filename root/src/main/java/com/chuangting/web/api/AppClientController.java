package com.chuangting.web.api;

import com.chuangtang.client.domain.AppClientDO;
import com.chuangtang.client.query.AppClientQueryDO;
import com.chuangtang.client.service.IAppClientService;
import com.chuangting.common.result.ResultDO;
import com.chuangting.common.result.ResultSupport;
import com.chuangting.common.result.web.ObjectResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/api")
@Api(value = "AppClientController | 客户端验证获取令牌")
public class AppClientController {

    @Autowired
    private IAppClientService appClientService;


    @RequestMapping(value="/token.html", method = RequestMethod.POST)
    @ApiOperation(value="验证客户端", notes="客户端通过客户端的名字和安全码获得认证然后得到TOKEN")
    @ApiImplicitParam(paramType="AppClientDO", name = "domain", value = "客户端验证对象", required = true)
    public @ResponseBody ObjectResponse authentication(HttpServletRequest request, @RequestBody AppClientDO domain) {

        AppClientQueryDO queryDO = new AppClientQueryDO();

        System.out.println(domain);
        queryDO.setAppName(domain.getAppName());
        queryDO.setSecret(domain.getSecret());
        queryDO.setVersion(domain.getVersion());

        ObjectResponse<AppClientDO> response = new ObjectResponse<>();

        ResultDO result = appClientService.authentication(queryDO);
        if(result.isSuccess()) {
            response.setStatus(200);
            response.setData((AppClientDO)result.getModel(ResultSupport.FIRST_MODEL_KEY));
            response.setVersion(1);
        } else {
            response.setStatus(result.getErrorCode().getCode());
            response.setMsg(result.getErrorCode().getMsg());
            response.setData((AppClientDO)result.getModel(ResultSupport.FIRST_MODEL_KEY));
            response.setVersion(1);
        }
        return response;
    }
}
