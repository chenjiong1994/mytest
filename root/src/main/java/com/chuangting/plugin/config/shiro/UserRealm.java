package com.chuangting.plugin.config.shiro;

import com.chuangting.admin.domain.AdminLoginDO;
import com.chuangting.admin.domain.PermissionsDO;

import com.chuangting.admin.query.AdminQueryDO;
import com.chuangting.admin.service.IAdminService;
import com.chuangting.admin.service.IRoleService;
import com.chuangting.common.result.ResultDO;
import com.chuangting.common.result.ResultSupport;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class UserRealm extends AuthorizingRealm {

    @Autowired
    IAdminService adminService;

    @Autowired
    IRoleService roleService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {

        String username = (String) principals.getPrimaryPrincipal();
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();

        AdminQueryDO queryDO = new AdminQueryDO();
        queryDO.setUsername(username);
        ResultDO result = adminService.query(queryDO);

        if(!result.isSuccess()) {
            return null;
        }
        List<AdminLoginDO> list = (List<AdminLoginDO>)result.getModel(ResultSupport.FIRST_MODEL_KEY);
        AdminLoginDO domain = list.get(0);

        String roleName = domain.getRole().getRole();
        Set<String> permissiones = new HashSet<>();

        for(PermissionsDO domain2 : domain.getRole().getPermissiones()) {
            permissiones.add(domain2.getPermissions());
        }
        authorizationInfo.addRole(roleName);
        authorizationInfo.addStringPermissions(permissiones);
        return authorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) token;
        AdminQueryDO queryDO = new AdminQueryDO();
        queryDO.setUsername(usernamePasswordToken.getUsername());
        ResultDO result = adminService.login(queryDO);
        if(!result.isSuccess()) {
            throw new AuthenticationException(result.getErrorCode().getMsg());
        }
        AdminLoginDO domain = (AdminLoginDO)result.getModel(ResultSupport.FIRST_MODEL_KEY);
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(domain.getUsername(), domain.getPassword(), ByteSource.Util.bytes(domain.getSalt()), getName());
        return authenticationInfo;
    }
}
