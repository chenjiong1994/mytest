package com.chuangting.house.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "房源细节查询对象模型")
public class HouseDetailQueryDO {

    @ApiModelProperty(value = "房屋ID",required = false)
    private Integer hHouseId;

    public Integer gethHouseId() {
        return hHouseId;
    }

    public void sethHouseId(Integer hHouseId) {
        this.hHouseId = hHouseId;
    }
}
