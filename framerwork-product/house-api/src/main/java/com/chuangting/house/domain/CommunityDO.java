package com.chuangting.house.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "社区对象模型")
public class CommunityDO {

    @ApiModelProperty(value = "id",required = false)
    private Integer id;

    @ApiModelProperty(value = "社区名",required = false)
    private String name;

    @ApiModelProperty(value ="省份",required = false)
    private String province;

    @ApiModelProperty(value ="城市" ,required = false)
    private String city;

    @ApiModelProperty(value = "区域",required = false)
    private String area;

    @ApiModelProperty(value = "地址",required = false)
     private String address;

    @ApiModelProperty(value = "纬度",required = false)
    private Double latitude;

    @ApiModelProperty(value = "经度",required = false)
    private Double longitude;

    @ApiModelProperty(value = "gps纬度",required = false)
    private Double latitudeGps;

    @ApiModelProperty(value = "gps经度",required = false)
    private Double longitudeGps;

    @ApiModelProperty(value = "类型",required = false)
    private String type;

    @ApiModelProperty(value = "管理费",required = false)
    private String managementFee;

    @ApiModelProperty(value = "大小",required = false)
    private String size;

    @ApiModelProperty(value = "总房数",required = false)
    private String houses;

    @ApiModelProperty(value = "建成时间",required = false)
    private String year;

    @ApiModelProperty(value = "附近公园",required = false)
    private String parkings;

    @ApiModelProperty(value ="量" ,required = false)
    private String volume;

    @ApiModelProperty(value = "绿化",required = false)
    private String greening;

    @ApiModelProperty(value = "建造者",required = false)
    private String producer;

    @ApiModelProperty(value ="物管" ,required = false)
    private String management;

    @ApiModelProperty(value = "附近学校",required = false)
    private String school;

    @ApiModelProperty(value = "其他信息",required = false)
    private String info;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitudeGps() {
        return latitudeGps;
    }

    public void setLatitudeGps(Double latitudeGps) {
        this.latitudeGps = latitudeGps;
    }

    public Double getLongitudeGps() {
        return longitudeGps;
    }

    public void setLongitudeGps(Double longitudeGps) {
        this.longitudeGps = longitudeGps;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getManagementFee() {
        return managementFee;
    }

    public void setManagementFee(String managementFee) {
        this.managementFee = managementFee;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getHouses() {
        return houses;
    }

    public void setHouses(String houses) {
        this.houses = houses;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getParkings() {
        return parkings;
    }

    public void setParkings(String parkings) {
        this.parkings = parkings;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getGreening() {
        return greening;
    }

    public void setGreening(String greening) {
        this.greening = greening;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getManagement() {
        return management;
    }

    public void setManagement(String management) {
        this.management = management;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }



}
