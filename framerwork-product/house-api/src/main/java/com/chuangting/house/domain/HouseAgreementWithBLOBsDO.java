package com.chuangting.house.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("租房签名及合同内容对象模型")
public class HouseAgreementWithBLOBsDO {

    @ApiModelProperty(value = "合同内容",required = false)
    private String agreementContent;

    @ApiModelProperty(value = "服务合同html",required = false)
    private String serviceAgreementContent;

    @ApiModelProperty(value = "房东签名",required = false)
    private String landlordCheckId;

    @ApiModelProperty(value ="租客签名" ,required = false)
    private String memberCheckId;

    public String getAgreementContent() {
        return agreementContent;
    }

    public void setAgreementContent(String agreementContent) {
        this.agreementContent = agreementContent;
    }

    public String getServiceAgreementContent() {
        return serviceAgreementContent;
    }

    public void setServiceAgreementContent(String serviceAgreementContent) {
        this.serviceAgreementContent = serviceAgreementContent;
    }

    public String getLandlordCheckId() {
        return landlordCheckId;
    }

    public void setLandlordCheckId(String landlordCheckId) {
        this.landlordCheckId = landlordCheckId;
    }

    public String getMemberCheckId() {
        return memberCheckId;
    }

    public void setMemberCheckId(String memberCheckId) {
        this.memberCheckId = memberCheckId;
    }
}
