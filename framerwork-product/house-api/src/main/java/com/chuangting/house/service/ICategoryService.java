package com.chuangting.house.service;

import com.chuangting.house.domain.CategoryDO;
import com.chuangting.common.result.ResultDO;
import com.chuangting.house.query.CategoryQueryDO;

public interface ICategoryService {

    public ResultDO add(CategoryDO domain);

    public ResultDO edit(CategoryDO domain);

    public ResultDO remove(Integer id);

    public ResultDO get(Integer id);

    public ResultDO query(CategoryQueryDO queryDO);
}
