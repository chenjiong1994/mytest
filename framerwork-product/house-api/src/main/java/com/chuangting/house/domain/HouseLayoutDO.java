package com.chuangting.house.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ApiModel("房源布局对象模型")
public class HouseLayoutDO {

    @ApiModelProperty(value ="id" ,required = false)
    private Integer id;

    @ApiModelProperty(value = "房屋风格文字描述",required = false)
    private String name;

    @ApiModelProperty(value = "风格值描述添加数据的时候手动编码",required = false)
    private Integer layoutType;

    @ApiModelProperty(value = "创建时间",required = false)
    private Date createtime;

    @ApiModelProperty(value ="修改时间" ,required = false)
    private Date modifitime;

    @ApiModelProperty(value = "作者",required = false)
    private String author;

    @ApiModelProperty(value = "修改者",required = false)
    private String modifi;

    @ApiModelProperty(value = "版本号",required = false)
    private Integer version;

    @ApiModelProperty(value = "是否删除 -1删除 1有效",required = false)
    private Integer isDelete;

    @ApiModelProperty(value = "房屋id列表",required = false)
    private List<Integer> hHouseIds = new ArrayList<>();

    @ApiModelProperty(value = "房屋列表",required = false)
    private List<HouseDO> houses= new ArrayList<>();

    public List<Integer> gethHouseIds() {
        return hHouseIds;
    }

    public void sethHouseIds(List<Integer> hHouseIds) {
        this.hHouseIds = hHouseIds;
    }

    public List<HouseDO> getHouses() {
        return houses;
    }

    public void setHouses(List<HouseDO> houses) {
        this.houses = houses;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLayoutType() {
        return layoutType;
    }

    public void setLayoutType(Integer layoutType) {
        this.layoutType = layoutType;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getModifitime() {
        return modifitime;
    }

    public void setModifitime(Date modifitime) {
        this.modifitime = modifitime;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getModifi() {
        return modifi;
    }

    public void setModifi(String modifi) {
        this.modifi = modifi;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}
