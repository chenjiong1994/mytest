package com.chuangting.house.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

@ApiModel(value = "租房协议对象模型")
public class HouseAgreementDO {

    @ApiModelProperty(value = "id" ,required = false)
    private Integer id;

    @ApiModelProperty(value = "一致意见",required = false)
    private String agreementNo;

    @ApiModelProperty(value = "房东角色id",required = false)
    private Integer landlordId;

    @ApiModelProperty(value = "房源id",required = false)
    private Integer houseId;

    @ApiModelProperty(value = "用户角色id",required = false)
    private Integer memberId;

    @ApiModelProperty(value ="房东签署状态 1已签" ,required = false)
    private Integer landlordCheckStatus;

    @ApiModelProperty(value = "租客签署状态 1已签",required = false)
    private Integer memberCheckStatus;

    @ApiModelProperty(value ="房东签署时间" ,required = false)
    private Integer landlordCheckDate;

    @ApiModelProperty(value = "租客签署时间",required = false)
    private Integer memberCheckDate;

    @ApiModelProperty(value = "远程授权服务协议",required = false)
    private Integer remoteProtocolId;

    @ApiModelProperty(value = "授权合同",required = false)
    private Integer letterAuthorizationId;


    @ApiModelProperty(value = "租房合同页数",required = false)
    private Integer agreementPages;

    @ApiModelProperty(value = "合同地址（空白）",required = false)
    private Integer agreementTemplate;


    @ApiModelProperty(value = "租客签署合同",required = false)
    private Integer memberCheckAgreement;

    @ApiModelProperty(value = "房东签署合同",required = false)
    private Integer landlordCheckAgreement;

    @ApiModelProperty(value = "本地贷款合同",required = false)
    private Integer loanAgreement;

    @ApiModelProperty(value = "本地签章贷款合同",required = false)
    private Integer checkLoanAgreement;

    @ApiModelProperty(value = "贷款合同公司签章",required = false)
    private Integer compnyLoanAgreement;

    @ApiModelProperty(value ="远程贷款合同" ,required = false)
    private Integer remoteLoanAgreement;

    @ApiModelProperty(value = "远程签章贷款合同",required = false)
    private Integer checkRemoteLoanAgreement;

    @ApiModelProperty(value = "远程贷款合同公司签章",required = false)
    private Integer compnyRemoteLoanAgreement;

    @ApiModelProperty(value ="空白服务合同" ,required = false)
    private Integer serviceAgreementTemplate;

    @ApiModelProperty(value = "公司签署服务协议",required = false)
    private Integer checkCompnyServiceAgreement;

    @ApiModelProperty(value = "房东平台服务合同",required = false)
    private Integer landlordServiceAgreement;

    @ApiModelProperty(value = "服务合同页数",required = false)
    private Integer serviceAgreementPages;

    @ApiModelProperty(value = "租金",required = false)
    private BigDecimal price;

    @ApiModelProperty(value = "押金",required = false)
    private BigDecimal pledge;

    @ApiModelProperty(value = "签的过期时间 签署过程和签署成功共用",required = false)
    private Date checkTimeout;

    @ApiModelProperty(value ="租期" ,required = false)
    private Integer tenancy;

    @ApiModelProperty(value = "合同生效时间",required = false)
    private Date effectiveDate;

    @ApiModelProperty(value = "押金租金支付 关联支付ID",required = false)
    private Long paymentId;

    @ApiModelProperty(value = "贷款状态",required = false)
    private String loanStatus;

    @ApiModelProperty(value = "0未支付 1已支付",required = false)
    private Integer paymentStatus;

    @ApiModelProperty(value = "小贷项目id",required = false)
    private Long projectId;

    @ApiModelProperty(value = "添加时间",required = false)
    private Date createtime;

    @ApiModelProperty(value = "退租发起人 房东或租客id",required = false)
    private Integer cancelId;

    @ApiModelProperty(value ="发起退租时间" ,required = false)
    private Date cancelDate;

    @ApiModelProperty(value = "退租过期时间",required = false)
    private Date cancelTimeout;

    @ApiModelProperty(value ="同意退租 房东退的钱" ,required = false)
    private BigDecimal landlordRefund;

    @ApiModelProperty(value ="银行打款流水号" ,required = false)
    private String bankRecordNo;

    @ApiModelProperty(value ="同意退租租客退的钱" ,required = false)
    private BigDecimal memberRefund;

    @ApiModelProperty(value = "银行打款流水号",required = false)
    private String bankRecordType;

    @ApiModelProperty(value = "财务打款确认时间",required = false)
    private Date bankPaymentDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAgreementNo() {
        return agreementNo;
    }

    public void setAgreementNo(String agreementNo) {
        this.agreementNo = agreementNo;
    }

    public Integer getLandlordId() {
        return landlordId;
    }

    public void setLandlordId(Integer landlordId) {
        this.landlordId = landlordId;
    }

    public Integer getHouseId() {
        return houseId;
    }

    public void setHouseId(Integer houseId) {
        this.houseId = houseId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Integer getLandlordCheckStatus() {
        return landlordCheckStatus;
    }

    public void setLandlordCheckStatus(Integer landlordCheckStatus) {
        this.landlordCheckStatus = landlordCheckStatus;
    }

    public Integer getMemberCheckStatus() {
        return memberCheckStatus;
    }

    public void setMemberCheckStatus(Integer memberCheckStatus) {
        this.memberCheckStatus = memberCheckStatus;
    }

    public Integer getLandlordCheckDate() {
        return landlordCheckDate;
    }

    public void setLandlordCheckDate(Integer landlordCheckDate) {
        this.landlordCheckDate = landlordCheckDate;
    }

    public Integer getMemberCheckDate() {
        return memberCheckDate;
    }

    public void setMemberCheckDate(Integer memberCheckDate) {
        this.memberCheckDate = memberCheckDate;
    }

    public Integer getRemoteProtocolId() {
        return remoteProtocolId;
    }

    public void setRemoteProtocolId(Integer remoteProtocolId) {
        this.remoteProtocolId = remoteProtocolId;
    }

    public Integer getLetterAuthorizationId() {
        return letterAuthorizationId;
    }

    public void setLetterAuthorizationId(Integer letterAuthorizationId) {
        this.letterAuthorizationId = letterAuthorizationId;
    }

    public Integer getAgreementPages() {
        return agreementPages;
    }

    public void setAgreementPages(Integer agreementPages) {
        this.agreementPages = agreementPages;
    }

    public Integer getAgreementTemplate() {
        return agreementTemplate;
    }

    public void setAgreementTemplate(Integer agreementTemplate) {
        this.agreementTemplate = agreementTemplate;
    }

    public Integer getMemberCheckAgreement() {
        return memberCheckAgreement;
    }

    public void setMemberCheckAgreement(Integer memberCheckAgreement) {
        this.memberCheckAgreement = memberCheckAgreement;
    }

    public Integer getLandlordCheckAgreement() {
        return landlordCheckAgreement;
    }

    public void setLandlordCheckAgreement(Integer landlordCheckAgreement) {
        this.landlordCheckAgreement = landlordCheckAgreement;
    }

    public Integer getLoanAgreement() {
        return loanAgreement;
    }

    public void setLoanAgreement(Integer loanAgreement) {
        this.loanAgreement = loanAgreement;
    }

    public Integer getCheckLoanAgreement() {
        return checkLoanAgreement;
    }

    public void setCheckLoanAgreement(Integer checkLoanAgreement) {
        this.checkLoanAgreement = checkLoanAgreement;
    }

    public Integer getCompnyLoanAgreement() {
        return compnyLoanAgreement;
    }

    public void setCompnyLoanAgreement(Integer compnyLoanAgreement) {
        this.compnyLoanAgreement = compnyLoanAgreement;
    }

    public Integer getRemoteLoanAgreement() {
        return remoteLoanAgreement;
    }

    public void setRemoteLoanAgreement(Integer remoteLoanAgreement) {
        this.remoteLoanAgreement = remoteLoanAgreement;
    }

    public Integer getCheckRemoteLoanAgreement() {
        return checkRemoteLoanAgreement;
    }

    public void setCheckRemoteLoanAgreement(Integer checkRemoteLoanAgreement) {
        this.checkRemoteLoanAgreement = checkRemoteLoanAgreement;
    }

    public Integer getCompnyRemoteLoanAgreement() {
        return compnyRemoteLoanAgreement;
    }

    public void setCompnyRemoteLoanAgreement(Integer compnyRemoteLoanAgreement) {
        this.compnyRemoteLoanAgreement = compnyRemoteLoanAgreement;
    }

    public Integer getServiceAgreementTemplate() {
        return serviceAgreementTemplate;
    }

    public void setServiceAgreementTemplate(Integer serviceAgreementTemplate) {
        this.serviceAgreementTemplate = serviceAgreementTemplate;
    }

    public Integer getCheckCompnyServiceAgreement() {
        return checkCompnyServiceAgreement;
    }

    public void setCheckCompnyServiceAgreement(Integer checkCompnyServiceAgreement) {
        this.checkCompnyServiceAgreement = checkCompnyServiceAgreement;
    }

    public Integer getLandlordServiceAgreement() {
        return landlordServiceAgreement;
    }

    public void setLandlordServiceAgreement(Integer landlordServiceAgreement) {
        this.landlordServiceAgreement = landlordServiceAgreement;
    }

    public Integer getServiceAgreementPages() {
        return serviceAgreementPages;
    }

    public void setServiceAgreementPages(Integer serviceAgreementPages) {
        this.serviceAgreementPages = serviceAgreementPages;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPledge() {
        return pledge;
    }

    public void setPledge(BigDecimal pledge) {
        this.pledge = pledge;
    }

    public Date getCheckTimeout() {
        return checkTimeout;
    }

    public void setCheckTimeout(Date checkTimeout) {
        this.checkTimeout = checkTimeout;
    }

    public Integer getTenancy() {
        return tenancy;
    }

    public void setTenancy(Integer tenancy) {
        this.tenancy = tenancy;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Long paymentId) {
        this.paymentId = paymentId;
    }

    public String getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(String loanStatus) {
        this.loanStatus = loanStatus;
    }

    public Integer getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(Integer paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Integer getCancelId() {
        return cancelId;
    }

    public void setCancelId(Integer cancelId) {
        this.cancelId = cancelId;
    }

    public Date getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(Date cancelDate) {
        this.cancelDate = cancelDate;
    }

    public Date getCancelTimeout() {
        return cancelTimeout;
    }

    public void setCancelTimeout(Date cancelTimeout) {
        this.cancelTimeout = cancelTimeout;
    }

    public BigDecimal getLandlordRefund() {
        return landlordRefund;
    }

    public void setLandlordRefund(BigDecimal landlordRefund) {
        this.landlordRefund = landlordRefund;
    }

    public String getBankRecordNo() {
        return bankRecordNo;
    }

    public void setBankRecordNo(String bankRecordNo) {
        this.bankRecordNo = bankRecordNo;
    }

    public BigDecimal getMemberRefund() {
        return memberRefund;
    }

    public void setMemberRefund(BigDecimal memberRefund) {
        this.memberRefund = memberRefund;
    }

    public String getBankRecordType() {
        return bankRecordType;
    }

    public void setBankRecordType(String bankRecordType) {
        this.bankRecordType = bankRecordType;
    }

    public Date getBankPaymentDate() {
        return bankPaymentDate;
    }

    public void setBankPaymentDate(Date bankPaymentDate) {
        this.bankPaymentDate = bankPaymentDate;
    }
}
