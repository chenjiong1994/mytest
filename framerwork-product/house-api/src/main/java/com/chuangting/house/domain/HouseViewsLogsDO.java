package com.chuangting.house.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel(value = "房源查看日志对象模块")
public class HouseViewsLogsDO {

    @ApiModelProperty(value = "id",required = false)
    private Integer id;

    @ApiModelProperty(value = "房屋ID",required = false)
    private Integer houseId;

    @ApiModelProperty(value ="用户ID" ,required = false)
    private Integer memberId;

    @ApiModelProperty(value ="客户端名称 小程序 手机客户端 web网站 手机网站" ,required = false)
    private String clientName;

    @ApiModelProperty(value ="客户端类型" ,required = false)
    private Integer clientType;

    @ApiModelProperty(value ="设备名称 pc iphone andorid ipad" ,required = false)
    private String deviceName;

    @ApiModelProperty(value = "浏览时间",required = false)
    private Date viewsDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHouseId() {
        return houseId;
    }

    public void setHouseId(Integer houseId) {
        this.houseId = houseId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public Integer getClientType() {
        return clientType;
    }

    public void setClientType(Integer clientType) {
        this.clientType = clientType;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public Date getViewsDate() {
        return viewsDate;
    }

    public void setViewsDate(Date viewsDate) {
        this.viewsDate = viewsDate;
    }
}
