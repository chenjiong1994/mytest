package com.chuangting.house.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("房源外围位置对象模型")
public class HPeripheralPositionDO {

    @ApiModelProperty(value ="id" ,required = false)
    private Integer id;

    @ApiModelProperty(value = "房子的ID",required = false)
    private Integer hHouseId;

    @ApiModelProperty(value = "建筑物的ID对应建筑物表如车站",required = false)
    private Integer buildId;

    @ApiModelProperty(value ="建筑物名字" ,required = false)
    private String name;

    @ApiModelProperty(value = "类型 1.地铁站 2.公交站 3.火车站 4.机场 5.长途汽车站 6.医院 7.学校",required = false)
    private String type;

    @ApiModelProperty(value ="距离公里数" ,required = false)
    private Integer distance;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer gethHouseId() {
        return hHouseId;
    }

    public void sethHouseId(Integer hHouseId) {
        this.hHouseId = hHouseId;
    }

    public Integer getBuildId() {
        return buildId;
    }

    public void setBuildId(Integer buildId) {
        this.buildId = buildId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }
}
