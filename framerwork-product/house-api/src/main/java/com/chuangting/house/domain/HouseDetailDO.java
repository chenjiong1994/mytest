package com.chuangting.house.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel(value = "房源细节对象模型")
public class HouseDetailDO {

    @ApiModelProperty(value = "房屋ID",required = false)
    private Integer hHouseId;

    @ApiModelProperty(value = "房屋ID",required = false)
    private Integer addEmployeeId;

    @ApiModelProperty(value = "房产证号码",required = false)
    private String certificateNumber;

    @ApiModelProperty(value = "房产证附件对应附件ID",required = false)
    private Integer certificateAttach;

    @ApiModelProperty(value = "房本人工审核 0未审核 1不通过 2已通过",required = false)
    private Integer certificateCheck;

    @ApiModelProperty(value = "锁类型",required = false)
    private Integer lockType;

    @ApiModelProperty(value ="锁sn" ,required = false)
    private String lockSerialNumber;

    @ApiModelProperty(value = "锁图片ID",required = false)
    private Integer lockAttachId;

    @ApiModelProperty(value = "锁人工审核 0未审核 1不通过 2已通过",required = false)
    private Byte lockCheck;

    @ApiModelProperty(value ="楼层" ,required = false)
    private Integer floor;

    @ApiModelProperty(value = "总楼层",required = false)
    private Integer allFloor;

    @ApiModelProperty(value = "门牌号",required = false)
    private String streetNumber;

    @ApiModelProperty(value ="朝向" ,required = false)
    private Integer direction;

    @ApiModelProperty(value = "朝向中文描述",required = false)
    private String directionName;

    @ApiModelProperty(value = "抵押形式 从字典表获取中文",required = false)
    private String pledge;

    @ApiModelProperty(value ="房屋格局 从格局表获得中文" ,required = false)
    private String room;

    @ApiModelProperty(value ="入住时间" ,required = false)
    private Date checkInTime;

    @ApiModelProperty(value = "介绍",required = false)
    private String introduce;

    @ApiModelProperty(value ="创建时间" ,required = false)
    private Date createtime;

    @ApiModelProperty(value = "修改时间",required = false)
    private Date modifitime;

    @ApiModelProperty(value = "作者",required = false)
    private String author;

    @ApiModelProperty(value ="修改者" ,required = false)
    private String modifi;

    @ApiModelProperty(value ="版本号" ,required = false)
    private Integer version;

    @ApiModelProperty(value = "是否删除 -1删除 1有效",required = false)
    private Integer isDelete;


    public Integer gethHouseId() {
        return hHouseId;
    }

    public void sethHouseId(Integer hHouseId) {
        this.hHouseId = hHouseId;
    }

    public Integer getAddEmployeeId() {
        return addEmployeeId;
    }

    public void setAddEmployeeId(Integer addEmployeeId) {
        this.addEmployeeId = addEmployeeId;
    }

    public String getCertificateNumber() {
        return certificateNumber;
    }

    public void setCertificateNumber(String certificateNumber) {
        this.certificateNumber = certificateNumber;
    }

    public Integer getCertificateAttach() {
        return certificateAttach;
    }

    public void setCertificateAttach(Integer certificateAttach) {
        this.certificateAttach = certificateAttach;
    }

    public Integer getCertificateCheck() {
        return certificateCheck;
    }

    public void setCertificateCheck(Integer certificateCheck) {
        this.certificateCheck = certificateCheck;
    }

    public Integer getLockType() {
        return lockType;
    }

    public void setLockType(Integer lockType) {
        this.lockType = lockType;
    }

    public String getLockSerialNumber() {
        return lockSerialNumber;
    }

    public void setLockSerialNumber(String lockSerialNumber) {
        this.lockSerialNumber = lockSerialNumber;
    }

    public Integer getLockAttachId() {
        return lockAttachId;
    }

    public void setLockAttachId(Integer lockAttachId) {
        this.lockAttachId = lockAttachId;
    }

    public Byte getLockCheck() {
        return lockCheck;
    }

    public void setLockCheck(Byte lockCheck) {
        this.lockCheck = lockCheck;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public Integer getAllFloor() {
        return allFloor;
    }

    public void setAllFloor(Integer allFloor) {
        this.allFloor = allFloor;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    public String getDirectionName() {
        return directionName;
    }

    public void setDirectionName(String directionName) {
        this.directionName = directionName;
    }

    public String getPledge() {
        return pledge;
    }

    public void setPledge(String pledge) {
        this.pledge = pledge;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public Date getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(Date checkInTime) {
        this.checkInTime = checkInTime;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getModifitime() {
        return modifitime;
    }

    public void setModifitime(Date modifitime) {
        this.modifitime = modifitime;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getModifi() {
        return modifi;
    }

    public void setModifi(String modifi) {
        this.modifi = modifi;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}
