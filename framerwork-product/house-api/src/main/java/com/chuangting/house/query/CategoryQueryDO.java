package com.chuangting.house.query;

import com.chuangting.common.domain.BaseQueryDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "分类对象查询模型")
public class CategoryQueryDO extends BaseQueryDO {

    @ApiModelProperty(value = "类目父id", required = true)
    private Integer hCategoryId;

    public Integer gethCategoryId() {
        return hCategoryId;
    }

    public void sethCategoryId(Integer hCategoryId) {
        this.hCategoryId = hCategoryId;
    }
}
