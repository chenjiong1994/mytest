package com.chuangting.house.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "家具对象查询表")
public class FurnitureQueryDO {

    @ApiModelProperty(value = "房源id",required = false)
    private Integer hHouseId;


    public Integer gethHouseId() {
        return hHouseId;
    }

    public void sethHouseId(Integer hHouseId) {
        this.hHouseId = hHouseId;
    }
}
