package com.chuangting.house.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ApiModel(value="分类对象模型")
public class CategoryDO {

    @ApiModelProperty(value = "id", required = false)
    private Integer id;

    @ApiModelProperty(value = "类目父id",required = false)
    private Integer hCategoryId;

    @ApiModelProperty(value = "目录CODE",required = false)
    private String code;

    @ApiModelProperty(value = "类目名字" ,required = false)
    private  String name;

    @ApiModelProperty(value = "类型",required = false)
    private  Integer type;

    @ApiModelProperty(value = "创建时间",required = false)
    private Date createtime;

    @ApiModelProperty(value = "修改时间",required = false)
    private Date modifitime;

    @ApiModelProperty(value = "作者",required = false)
    private String author;

    @ApiModelProperty(value = "修改者",required = false)
    private Integer version;

    @ApiModelProperty(value = "是否删除 -1删除 1有效",required = false)
    private Integer isDelete;

    @ApiModelProperty(value = "房源id列表",required = false)
    List<Integer> hHouseId = new ArrayList<>();

    @ApiModelProperty(value = "房源列表",required = false)
    List<HouseDO> houses = new ArrayList<>();

    public List<Integer> gethHouseId() {
        return hHouseId;
    }

    public void sethHouseId(List<Integer> hHouseId) {
        this.hHouseId = hHouseId;
    }

    public List<HouseDO> getHouses() {
        return houses;
    }

    public void setHouses(List<HouseDO> houses) {
        this.houses = houses;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getModifitime() {
        return modifitime;
    }

    public void setModifitime(Date modifitime) {
        this.modifitime = modifitime;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Integer gethCategoryId() {
        return hCategoryId;
    }

    public void sethCategoryId(Integer hCategoryId) {
        this.hCategoryId = hCategoryId;
    }

    @Override
    public String toString() {
        return "CategoryDO{" +
                "id=" + id +
                ", hCategoryId=" + hCategoryId +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", createtime=" + createtime +
                ", modifitime=" + modifitime +
                ", author='" + author + '\'' +
                ", version=" + version +
                ", isDelete=" + isDelete +
                '}';
    }
}
