package com.chuangting.house.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "房源查看日志查询对象模块")
public class HouseViewsLogsQueryDO {

    @ApiModelProperty(value = "房屋ID",required = false)
    private Integer houseId;

    @ApiModelProperty(value ="用户ID" ,required = false)
    private Integer memberId;

    public Integer getHouseId() {
        return houseId;
    }

    public void setHouseId(Integer houseId) {
        this.houseId = houseId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }
}
