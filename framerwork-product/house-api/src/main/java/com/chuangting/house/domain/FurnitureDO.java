package com.chuangting.house.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

@ApiModel(value = "家具对象模型")
public class FurnitureDO {

    @ApiModelProperty(value = "id",required = false)
    private Integer id;

    @ApiModelProperty(value = "房源id",required = false)
    private Integer hHouseId;



    @ApiModelProperty(value = "家具名",required = false)
    private String name;

    @ApiModelProperty(value = "价格",required = false)
    private BigDecimal price;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer gethHouseId() {
        return hHouseId;
    }

    public void sethHouseId(Integer hHouseId) {
        this.hHouseId = hHouseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "FurnitureDO{" +
                "id=" + id +
                ", hHouseId=" + hHouseId +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
