package com.chuangting.house.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "租房协议查询对象模型")
public class HouseAgreementQueryDO {

    @ApiModelProperty(value = "房源id",required = false)
    private Integer houseId;

    public Integer getHouseId() {
        return houseId;
    }

    public void setHouseId(Integer houseId) {
        this.houseId = houseId;
    }
}
