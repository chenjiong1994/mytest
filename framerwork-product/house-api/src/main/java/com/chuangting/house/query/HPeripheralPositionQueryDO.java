package com.chuangting.house.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "房源外围位置对象模型")
public class HPeripheralPositionQueryDO {

    @ApiModelProperty(value = "房子的ID",required = false)
    private Integer hHouseId;

    public Integer gethHouseId() {
        return hHouseId;
    }

    public void sethHouseId(Integer hHouseId) {
        this.hHouseId = hHouseId;
    }
}
