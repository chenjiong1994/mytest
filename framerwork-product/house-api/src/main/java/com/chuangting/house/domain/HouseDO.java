package com.chuangting.house.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@ApiModel("房源对象模块")
public class HouseDO {

    @ApiModelProperty(value = "id",required = false)
    private Integer id;

    @ApiModelProperty(value = "房源布局id",required = false)
    private Integer hHouseLayoutId;

    @ApiModelProperty(value ="房源布局" ,required = false)
    private HouseLayoutDO  HouseLayout = new HouseLayoutDO();

    @ApiModelProperty(value = "房东id",required = false)
    private Integer landlordId;

    @ApiModelProperty(value ="中介ID" ,required = false)
    private Integer agentId;

    @ApiModelProperty(value = "二房东id",required = false)
    private Integer substituteId;

    @ApiModelProperty(value = "封面图片附件ID",required = false)
    private Integer coverId;

    @ApiModelProperty(value = "小区名字",required = false)
    private String houseName;

    @ApiModelProperty(value = "省",required = false)
    private String province;

    @ApiModelProperty(value = "市",required = false)
    private String city;

    @ApiModelProperty(value = "区",required = false)
    private String district;

    @ApiModelProperty(value = "街道",required = false)
    private String street;

    @ApiModelProperty(value = "经度",required = false)
    private Double longitude;

    @ApiModelProperty(value = "纬度",required = false)
    private Double latitude;

    @ApiModelProperty(value = "租聘类型 1.整租 2合租",required = false)
    private Integer rentType;

    @ApiModelProperty(value = "租聘类型中文",required = false)
    private String rentTypeName;

    @ApiModelProperty(value = "面积",required = false)
    private Integer area;

    @ApiModelProperty(value = "租金价格",required = false)
    private BigDecimal price;

    @ApiModelProperty(value = "0-待审核 1-已通过 2-已拒绝",required = false)
    private Integer check;

    @ApiModelProperty(value = "0-待出租 1-待合租 2-已出租 -1删除",required = false)
    private Integer status;

    @ApiModelProperty(value = "版本号",required = false)
    private Integer version;

    @ApiModelProperty(value = "是否删除 -1删除 1有效",required = false)
    private Integer isDelete;

    @ApiModelProperty(value = "分类id列表",required = false)
    private List<Integer> hCategoryIds = new ArrayList<>();

    @ApiModelProperty(value = "分类列表",required = false)
    private List<CategoryDO>  categorys = new ArrayList<>();

    @ApiModelProperty(value = "家具id列表",required = false)
    private List<Integer> hFurnitureId = new ArrayList<>();

    @ApiModelProperty(value = "家具列表",required = false)
    private List<FurnitureDO> furnitures = new ArrayList<>();

    @ApiModelProperty(value = "细节id列表",required = false)
    private List<Integer> addEmployeeIds = new ArrayList<>();

    @ApiModelProperty(value = "细节列表",required = false)
    private  List<HouseDetailDO> houseDetails= new ArrayList<>();

    @ApiModelProperty(value = "房源外围位置id列表",required = false)
    private List<Integer> HPeripheralPositionIds = new ArrayList<>();

    @ApiModelProperty(value = "房源外围位置列表",required = false)
    private List<HPeripheralPositionDO> hPeripheralPositions = new ArrayList<>();

    public List<Integer> gethFurnitureId() {
        return hFurnitureId;
    }

    public void sethFurnitureId(List<Integer> hFurnitureId) {
        this.hFurnitureId = hFurnitureId;
    }

    public List<FurnitureDO> getFurnitures() {
        return furnitures;
    }

    public void setFurnitures(List<FurnitureDO> furnitures) {
        this.furnitures = furnitures;
    }

    public List<Integer> getAddEmployeeIds() {
        return addEmployeeIds;
    }

    public void setAddEmployeeIds(List<Integer> addEmployeeIds) {
        this.addEmployeeIds = addEmployeeIds;
    }

    public List<HouseDetailDO> getHouseDetails() {
        return houseDetails;
    }

    public void setHouseDetails(List<HouseDetailDO> houseDetails) {
        this.houseDetails = houseDetails;
    }

    public List<Integer> getHPeripheralPositionIds() {
        return HPeripheralPositionIds;
    }

    public void setHPeripheralPositionIds(List<Integer> HPeripheralPositionIds) {
        this.HPeripheralPositionIds = HPeripheralPositionIds;
    }

    public List<HPeripheralPositionDO> gethPeripheralPositions() {
        return hPeripheralPositions;
    }

    public void sethPeripheralPositions(List<HPeripheralPositionDO> hPeripheralPositions) {
        this.hPeripheralPositions = hPeripheralPositions;
    }

    public HouseLayoutDO getHouseLayout() {
        return HouseLayout;
    }

    public void setHouseLayout(HouseLayoutDO houseLayout) {
        HouseLayout = houseLayout;
    }

    public List<Integer> gethCategoryIds() {
        return hCategoryIds;
    }

    public void sethCategoryIds(List<Integer> hCategoryIds) {
        this.hCategoryIds = hCategoryIds;
    }

    public List<CategoryDO> getCategorys() {
        return categorys;
    }

    public void setCategorys(List<CategoryDO> categorys) {
        this.categorys = categorys;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer gethHouseLayoutId() {
        return hHouseLayoutId;
    }

    public void sethHouseLayoutId(Integer hHouseLayoutId) {
        this.hHouseLayoutId = hHouseLayoutId;
    }

    public Integer getLandlordId() {
        return landlordId;
    }

    public void setLandlordId(Integer landlordId) {
        this.landlordId = landlordId;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public Integer getSubstituteId() {
        return substituteId;
    }

    public void setSubstituteId(Integer substituteId) {
        this.substituteId = substituteId;
    }

    public Integer getCoverId() {
        return coverId;
    }

    public void setCoverId(Integer coverId) {
        this.coverId = coverId;
    }

    public String getHouseName() {
        return houseName;
    }

    public void setHouseName(String houseName) {
        this.houseName = houseName;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Integer getRentType() {
        return rentType;
    }

    public void setRentType(Integer rentType) {
        this.rentType = rentType;
    }

    public String getRentTypeName() {
        return rentTypeName;
    }

    public void setRentTypeName(String rentTypeName) {
        this.rentTypeName = rentTypeName;
    }

    public Integer getArea() {
        return area;
    }

    public void setArea(Integer area) {
        this.area = area;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getCheck() {
        return check;
    }

    public void setCheck(Integer check) {
        this.check = check;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    public String toString() {
        return "HouseDO{" +
                "id=" + id +
                ", hHouseLayoutId=" + hHouseLayoutId +
                ", landlordId=" + landlordId +
                ", agentId=" + agentId +
                ", substituteId=" + substituteId +
                ", coverId=" + coverId +
                ", houseName='" + houseName + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", district='" + district + '\'' +
                ", street='" + street + '\'' +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", rentType=" + rentType +
                ", rentTypeName='" + rentTypeName + '\'' +
                ", area=" + area +
                ", price=" + price +
                ", check=" + check +
                ", status=" + status +
                ", version=" + version +
                ", isDelete=" + isDelete +
                '}';
    }
}
