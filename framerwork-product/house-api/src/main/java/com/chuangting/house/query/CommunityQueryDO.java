package com.chuangting.house.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "小区对象查询模型")
public class CommunityQueryDO {

    @ApiModelProperty(value = "id",required = false)
    private Integer id;

    @ApiModelProperty(value = "社区名",required = false)
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
