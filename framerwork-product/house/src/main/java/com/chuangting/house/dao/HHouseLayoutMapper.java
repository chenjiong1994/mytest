package com.chuangting.house.dao;

import com.chuangting.house.entity.HHouseLayout;
import com.chuangting.house.entity.HHouseLayoutExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface HHouseLayoutMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table h_house_layout
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    int countByExample(HHouseLayoutExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table h_house_layout
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    int deleteByExample(HHouseLayoutExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table h_house_layout
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table h_house_layout
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    int insert(HHouseLayout record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table h_house_layout
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    int insertSelective(HHouseLayout record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table h_house_layout
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    List<HHouseLayout> selectByExample(HHouseLayoutExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table h_house_layout
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    HHouseLayout selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table h_house_layout
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    int updateByExampleSelective(@Param("record") HHouseLayout record, @Param("example") HHouseLayoutExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table h_house_layout
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    int updateByExample(@Param("record") HHouseLayout record, @Param("example") HHouseLayoutExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table h_house_layout
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    int updateByPrimaryKeySelective(HHouseLayout record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table h_house_layout
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    int updateByPrimaryKey(HHouseLayout record);
}