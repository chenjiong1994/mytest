package com.chuangting.house.entity;

import java.util.Date;

public class HCategory {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column h_category.id
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column h_category.h_category_id
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    private Integer hCategoryId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column h_category.code
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    private String code;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column h_category.name
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    private String name;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column h_category.type
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    private Integer type;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column h_category.createtime
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    private Date createtime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column h_category.modifitime
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    private Date modifitime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column h_category.author
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    private String author;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column h_category.modifi
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    private String modifi;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column h_category.version
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    private Integer version;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column h_category.is_delete
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    private Integer isDelete;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column h_category.id
     *
     * @return the value of h_category.id
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column h_category.id
     *
     * @param id the value for h_category.id
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column h_category.h_category_id
     *
     * @return the value of h_category.h_category_id
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    public Integer gethCategoryId() {
        return hCategoryId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column h_category.h_category_id
     *
     * @param hCategoryId the value for h_category.h_category_id
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    public void sethCategoryId(Integer hCategoryId) {
        this.hCategoryId = hCategoryId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column h_category.code
     *
     * @return the value of h_category.code
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    public String getCode() {
        return code;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column h_category.code
     *
     * @param code the value for h_category.code
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column h_category.name
     *
     * @return the value of h_category.name
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column h_category.name
     *
     * @param name the value for h_category.name
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column h_category.type
     *
     * @return the value of h_category.type
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    public Integer getType() {
        return type;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column h_category.type
     *
     * @param type the value for h_category.type
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column h_category.createtime
     *
     * @return the value of h_category.createtime
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    public Date getCreatetime() {
        return createtime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column h_category.createtime
     *
     * @param createtime the value for h_category.createtime
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column h_category.modifitime
     *
     * @return the value of h_category.modifitime
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    public Date getModifitime() {
        return modifitime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column h_category.modifitime
     *
     * @param modifitime the value for h_category.modifitime
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    public void setModifitime(Date modifitime) {
        this.modifitime = modifitime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column h_category.author
     *
     * @return the value of h_category.author
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    public String getAuthor() {
        return author;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column h_category.author
     *
     * @param author the value for h_category.author
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column h_category.modifi
     *
     * @return the value of h_category.modifi
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    public String getModifi() {
        return modifi;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column h_category.modifi
     *
     * @param modifi the value for h_category.modifi
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    public void setModifi(String modifi) {
        this.modifi = modifi;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column h_category.version
     *
     * @return the value of h_category.version
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column h_category.version
     *
     * @param version the value for h_category.version
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column h_category.is_delete
     *
     * @return the value of h_category.is_delete
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    public Integer getIsDelete() {
        return isDelete;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column h_category.is_delete
     *
     * @param isDelete the value for h_category.is_delete
     *
     * @mbggenerated Tue Sep 17 15:26:31 CST 2019
     */
    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}