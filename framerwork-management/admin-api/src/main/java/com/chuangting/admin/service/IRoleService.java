package com.chuangting.admin.service;

import com.chuangting.admin.domain.RoleDO;
import com.chuangting.admin.query.RoleQueryDO;
import com.chuangting.common.result.ResultDO;

public interface IRoleService {

    public ResultDO add(RoleDO domain);

    public ResultDO edit(RoleDO domain);

    public ResultDO remove(Integer id);

    public ResultDO get(Integer id);

    public ResultDO query(RoleQueryDO queryDO);
}
