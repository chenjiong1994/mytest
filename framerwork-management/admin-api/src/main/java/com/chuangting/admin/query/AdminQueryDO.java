package com.chuangting.admin.query;

import com.chuangting.common.domain.BaseQueryDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "管理员查询对象模型")
public class AdminQueryDO  extends BaseQueryDO {

    @ApiModelProperty(value = "管理员登录名", required = true)
    private String username;

    @ApiModelProperty(value = "管理员密码", required = true)
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
