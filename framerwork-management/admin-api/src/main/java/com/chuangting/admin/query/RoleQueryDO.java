package com.chuangting.admin.query;

import com.chuangting.common.domain.BaseQueryDO;
import io.swagger.annotations.ApiModelProperty;

public class RoleQueryDO  extends BaseQueryDO {

    @ApiModelProperty(value = "角色名", required = true)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
