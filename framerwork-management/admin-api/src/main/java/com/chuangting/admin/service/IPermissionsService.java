package com.chuangting.admin.service;

import com.chuangting.admin.domain.PermissionsDO;
import com.chuangting.admin.query.PermissionsQueryDO;
import com.chuangting.common.result.ResultDO;

public interface IPermissionsService {

    public ResultDO add(PermissionsDO domain);

    public ResultDO edit(PermissionsDO domain);

    public ResultDO remove(Integer id);

    public ResultDO get(Integer id);

    public ResultDO query(PermissionsQueryDO queryDO);
}
