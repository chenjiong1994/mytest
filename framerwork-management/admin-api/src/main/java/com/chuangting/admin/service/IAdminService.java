package com.chuangting.admin.service;

import com.chuangting.admin.domain.AdminLoginDO;
import com.chuangting.admin.query.AdminQueryDO;
import com.chuangting.common.result.ResultDO;

public interface IAdminService {

    public ResultDO login(AdminQueryDO queryDO);

    public ResultDO add(AdminLoginDO domain);

    public ResultDO edit(AdminLoginDO domain);

    public ResultDO remove(Integer id);

    public ResultDO get(Integer id);

    public ResultDO query(AdminQueryDO queryDO);
}
