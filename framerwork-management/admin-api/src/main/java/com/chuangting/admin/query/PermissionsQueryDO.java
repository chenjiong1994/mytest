package com.chuangting.admin.query;

import com.chuangting.common.domain.BaseQueryDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "权利查询对象模型")
public class PermissionsQueryDO extends BaseQueryDO {

    @ApiModelProperty(value = "角色ID", required = true)
    private Integer roleId;

    @ApiModelProperty(value = "权利类型 1--模块 2--按钮", required = true)
    private Integer type;

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
