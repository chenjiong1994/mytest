package com.chuangting.admin.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "管理员对象模型")
public class AdminLoginDO {

    @ApiModelProperty(value = "id", required = false)
    private Integer id;

    @ApiModelProperty(value = "管理员登录名", required = false)
    private String username;

    @ApiModelProperty(value = "管理员密码", required = false)
    private String password;

    @ApiModelProperty(value = "密码盐值", required = false)
    private String salt;

    @ApiModelProperty(value = "角色Id", required = false)
    private Integer roleId;

   @ApiModelProperty(value = "管理员角色", required = false)
    private RoleDO role;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public RoleDO getRole() {
        return role;
    }

    public void setRole(RoleDO role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "AdminLoginDO{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", roleId=" + roleId +
                ", role=" + role +
                '}';
    }
}
