package com.chuangting.admin.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

@ApiModel(value = "角色对象模型")
public class RoleDO {

    @ApiModelProperty(value = "id", required = false)
    private Integer id;

    @ApiModelProperty(value = "角色编码", required = false)
    private String role;

    @ApiModelProperty(value = "角色名", required = false)
    private String name;

    @ApiModelProperty(value = "备注", required = false)
    private String remarks;

    @ApiModelProperty(value = "权力ID列表", required = false)
    private List<Integer> permissionIds = new ArrayList<>();

    @ApiModelProperty(value = "权力列表", required = false)
    private List<PermissionsDO> permissiones = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public List<PermissionsDO> getPermissiones() {
        return permissiones;
    }

    public void setPermissiones(List<PermissionsDO> permissiones) {
        this.permissiones = permissiones;
    }

    public List<Integer> getPermissionIds() {
        return permissionIds;
    }

    public void setPermissionIds(List<Integer> permissionIds) {
        this.permissionIds = permissionIds;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
