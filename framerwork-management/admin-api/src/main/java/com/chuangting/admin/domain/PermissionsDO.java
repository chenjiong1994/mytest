package com.chuangting.admin.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "权利对象模型")
public class PermissionsDO {

    @ApiModelProperty(value = "id", required = false)
    private Integer id;

    @ApiModelProperty(value = "权限字符串", required = false)
    private String permissions;

    @ApiModelProperty(value = "权利名字", required = false)
    private String name;

    @ApiModelProperty(value = "权利路径", required = false)
    private String actionUrl;

    @ApiModelProperty(value = "权利类型 1--模块 2--按钮", required = false)
    private Integer type;
    @ApiModelProperty(value = "页面请求地址", required = false)
    private String requestUrl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActionUrl() {
        return actionUrl;
    }

    public void setActionUrl(String actionUrl) {
        this.actionUrl = actionUrl;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }
}
