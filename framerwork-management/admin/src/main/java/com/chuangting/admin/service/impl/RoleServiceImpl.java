package com.chuangting.admin.service.impl;

import com.chuangting.admin.dao.SysRoleMapper;
import com.chuangting.admin.dao.SysRolePermissionsMapper;
import com.chuangting.admin.domain.PermissionsDO;
import com.chuangting.admin.domain.RoleDO;
import com.chuangting.admin.entity.SysRole;
import com.chuangting.admin.entity.SysRoleExample;
import com.chuangting.admin.entity.SysRolePermissions;
import com.chuangting.admin.entity.SysRolePermissionsExample;
import com.chuangting.admin.query.RoleQueryDO;
import com.chuangting.admin.service.IPermissionsService;
import com.chuangting.admin.service.IRoleService;
import com.chuangting.common.enums.ErrorCode;
import com.chuangting.common.result.ResultDO;
import com.chuangting.common.result.ResultSupport;
import com.chuangting.common.util.BeanUtilsExtends;
import com.chuangting.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

<<<<<<< HEAD
@SuppressWarnings("all")
=======
>>>>>>> b0b392cd70b93e1ddfcdfe46b085ba21851d8fbf
@Service
public class RoleServiceImpl implements IRoleService {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private SysRolePermissionsMapper sysRolePermissionsMapper;

    @Autowired
    private IPermissionsService permissionsService;

    public ResultDO add(RoleDO domain) {

        SysRole entity = new SysRole();
        ResultDO result = BeanUtilsExtends.copy(entity, domain);

        if (!result.isSuccess()) {
            result.setSuccess(false);
            return result;
        }

        int r = -1;

        try {
            r = sysRoleMapper.insertSelective(entity);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.SYSTEM_EXCEPTION);
            return result;
        }

        if (r < 0) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.ADD_ROLE_ERROR);
            return result;
        }

        result = setPermissions(entity.getId(), domain.getPermissionIds());

        return result;
    }

    private ResultDO removePermissions(Integer id) {

        ResultDO result = new ResultSupport();

        SysRolePermissionsExample example = new SysRolePermissionsExample();
        example.createCriteria().andRoleIdEqualTo(id);

        int r = -1;
        try {
            r = sysRolePermissionsMapper.deleteByExample(example);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.SYSTEM_EXCEPTION);
            return result;
        }

        if (r < 0) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.DELETE_ROLE_PERMISSIONS_ERROR);
            return result;
        }

        return result;
    }

    private ResultDO setPermissions(Integer id, List<Integer> permissionIds) {

        ResultDO result = new ResultSupport();

        if (permissionIds.size() == 0) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.ROLE_PERMISSIONS_NULL);
            return result;
        }

        for (Integer permissionId : permissionIds) {
            SysRolePermissions entity = new SysRolePermissions();
            entity.setRoleId(id);
            entity.setPermissionsId(permissionId);
            try {
                sysRolePermissionsMapper.insertSelective(entity);
            } catch (Exception e) {
                result.setSuccess(false);
                result.setErrorCode(ErrorCode.ADD_ROLE_PERMISSIONS_ERROR);
                return result;
            }
        }

        return result;
    }

    private ResultDO getPermissiones(Integer id) {

        ResultDO result = new ResultSupport();

        SysRolePermissionsExample example = new SysRolePermissionsExample();
        example.createCriteria().andRoleIdEqualTo(id);

        List<SysRolePermissions> list = null;

        try {
            list = sysRolePermissionsMapper.selectByExample(example);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.ADD_ROLE_PERMISSIONS_ERROR);
            return result;
        }
        List<PermissionsDO> doList = new ArrayList<PermissionsDO>();
        for (SysRolePermissions entity : list) {
            result = permissionsService.get(entity.getId());
            if (!result.isSuccess()) {
                return result;
            }
            doList.add((PermissionsDO) result.getModel(ResultSupport.FIRST_MODEL_KEY));
        }
        result.setModel(ResultSupport.FIRST_MODEL_KEY, doList);
        return result;
    }

    public ResultDO edit(RoleDO domain) {

        SysRole entity = new SysRole();
        ResultDO result = BeanUtilsExtends.copy(entity, domain);

        if (!result.isSuccess()) {
            result.setSuccess(false);
            return result;
        }

        int r = -1;

        try {
            r = sysRoleMapper.updateByPrimaryKeySelective(entity);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.SYSTEM_EXCEPTION);
            return result;
        }

        if (r < 0) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.EDIT_ROLE_ERROR);
            return result;
        }

        if (domain.getPermissionIds().size() != 0) {
            result = removePermissions(domain.getId());
            if (!result.isSuccess()) {
                return result;
            }
            result = setPermissions(domain.getId(), domain.getPermissionIds());
        }

        return result;
    }

    public ResultDO remove(Integer id) {

        ResultDO result = new ResultSupport();

        int r = -1;

        try {
            r = sysRoleMapper.deleteByPrimaryKey(id);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.SYSTEM_EXCEPTION);
            return result;
        }

        if (r < 0) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.DELETE_ROLE_ERROR);
            return result;
        }
        result = removePermissions(id);
        return result;
    }

    public ResultDO get(Integer id) {

        SysRole entity = null;

        ResultDO result = new ResultSupport();

        try {
            entity = sysRoleMapper.selectByPrimaryKey(id);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.SYSTEM_EXCEPTION);
            return result;
        }
        RoleDO domain = getDomain(entity);

        if (domain == null) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.GET_ROLE_ERROR);
            return result;
        }
        result.setModel(ResultSupport.FIRST_MODEL_KEY, domain);
        return result;
    }

    public ResultDO query(RoleQueryDO queryDO) {

        ResultDO result = new ResultSupport();

        SysRoleExample example = new SysRoleExample();

        if (StringUtils.isNotEmpty(queryDO.getName())) {
            example.createCriteria().andNameLike("%" + queryDO.getName() + "%");
        }

        List<SysRole> list = null;
        try {
            list = sysRoleMapper.selectByExample(example);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.SYSTEM_EXCEPTION);
            return result;
        }

        List<RoleDO> doList = getDomainList(list);

        if (doList == null) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.SYSTEM_EXCEPTION);
            return result;
        }

        result.setModel(ResultSupport.FIRST_MODEL_KEY, doList);
        return result;
    }

    private RoleDO getDomain(SysRole entity) {
        if (entity == null) return null;
        RoleDO domain = new RoleDO();
        ResultDO result = getPermissiones(entity.getId());
        if (!result.isSuccess()) {
            return null;
        }
        if (!BeanUtilsExtends.copyProperties(domain, entity)) {
            return null;
        }
        domain.setPermissiones((List<PermissionsDO>) result.getModel(ResultSupport.FIRST_MODEL_KEY));
        return domain;
    }

    private List<RoleDO> getDomainList(List<SysRole> list) {
        List<RoleDO> doList = new ArrayList<RoleDO>();
        if (list.size() > 0) {
            for (SysRole entity : list) {
                RoleDO domain = this.getDomain(entity);
                if (domain != null) {
                    doList.add(domain);
                } else {
                    return null;
                }
            }
        } else {
            return null;
        }
        return doList;
    }

}
