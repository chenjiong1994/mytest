package com.chuangting.admin.dao;

import com.chuangting.admin.entity.SysAdminLogin;
import com.chuangting.admin.entity.SysAdminLoginExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface SysAdminLoginMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_admin_login
     *
     * @mbggenerated Tue Sep 17 08:54:23 CST 2019
     */
    int countByExample(SysAdminLoginExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_admin_login
     *
     * @mbggenerated Tue Sep 17 08:54:23 CST 2019
     */
    int deleteByExample(SysAdminLoginExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_admin_login
     *
     * @mbggenerated Tue Sep 17 08:54:23 CST 2019
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_admin_login
     *
     * @mbggenerated Tue Sep 17 08:54:23 CST 2019
     */
    int insert(SysAdminLogin record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_admin_login
     *
     * @mbggenerated Tue Sep 17 08:54:23 CST 2019
     */
    int insertSelective(SysAdminLogin record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_admin_login
     *
     * @mbggenerated Tue Sep 17 08:54:23 CST 2019
     */
    List<SysAdminLogin> selectByExample(SysAdminLoginExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_admin_login
     *
     * @mbggenerated Tue Sep 17 08:54:23 CST 2019
     */
    SysAdminLogin selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_admin_login
     *
     * @mbggenerated Tue Sep 17 08:54:23 CST 2019
     */
    int updateByExampleSelective(@Param("record") SysAdminLogin record, @Param("example") SysAdminLoginExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_admin_login
     *
     * @mbggenerated Tue Sep 17 08:54:23 CST 2019
     */
    int updateByExample(@Param("record") SysAdminLogin record, @Param("example") SysAdminLoginExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_admin_login
     *
     * @mbggenerated Tue Sep 17 08:54:23 CST 2019
     */
    int updateByPrimaryKeySelective(SysAdminLogin record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_admin_login
     *
     * @mbggenerated Tue Sep 17 08:54:23 CST 2019
     */
    int updateByPrimaryKey(SysAdminLogin record);
}