package com.chuangting.admin.service.impl;

import com.chuangting.admin.dao.SysAdminLoginMapper;
import com.chuangting.admin.domain.AdminLoginDO;
import com.chuangting.admin.domain.RoleDO;
import com.chuangting.admin.entity.SysAdminLogin;
import com.chuangting.admin.entity.SysAdminLoginExample;
import com.chuangting.admin.query.AdminQueryDO;
import com.chuangting.admin.service.IAdminService;
import com.chuangting.admin.service.IRoleService;
import com.chuangting.common.enums.ErrorCode;
import com.chuangting.common.result.ResultDO;
import com.chuangting.common.result.ResultSupport;
import com.chuangting.common.util.BeanUtilsExtends;
import com.chuangting.common.util.PasswordUtil;
import com.chuangting.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class AdminServiceImpl implements IAdminService {

    @Autowired
    private SysAdminLoginMapper sysAdminLoginMapper;

    @Autowired
    private IRoleService roleService;

    public ResultDO login(AdminQueryDO queryDO) {

        ResultDO result = new ResultSupport();

        SysAdminLoginExample example = new SysAdminLoginExample();

        if (StringUtils.isEmpty(queryDO.getUsername())) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.LOGIN_USERNAME_NULL);
            return result;
        }

        /*if (StringUtils.isEmpty(queryDO.getPassword())) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.LOGIN_PASSWORD_NULL);
            return result;
        }*/

        example.createCriteria()
                .andUsernameEqualTo(queryDO.getUsername());

        List<SysAdminLogin> list = null;

        try {
            list = sysAdminLoginMapper.selectByExample(example);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.SYSTEM_EXCEPTION);
            e.printStackTrace();
            return result;
        }

        if (list.size() == 0) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.LOGIN_ERROR);
        }

        AdminLoginDO domain = getDomain(list.get(0));

        if (domain == null) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.LOGIN_ERROR);
            return result;
        }
        result.setModel(ResultSupport.FIRST_MODEL_KEY, domain);
        return result;
    }

    private SysAdminLogin setPassword(SysAdminLogin entity) {
        Map<String, String> passMap = PasswordUtil.encrypt(entity.getPassword());
        entity.setSalt(passMap.get(PasswordUtil.SALE));
        entity.setPassword(passMap.get(PasswordUtil.PASSWORD));
        return entity;
    }

    public ResultDO add(AdminLoginDO domain) {

        SysAdminLogin entity = new SysAdminLogin();
        ResultDO result = BeanUtilsExtends.copy(entity, domain);

        if (!result.isSuccess()) {
            result.setSuccess(false);
            return result;
        }

        entity = setPassword(entity);

        int r = -1;

        try {
            r = sysAdminLoginMapper.insertSelective(entity);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.SYSTEM_EXCEPTION);
            e.printStackTrace();
            return result;
        }

        if (r < 0) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.ADD_ADMIN_ERROR);
            return result;
        }

        return result;
    }

    public ResultDO edit(AdminLoginDO domain) {

        SysAdminLogin entity = new SysAdminLogin();

        ResultDO result = BeanUtilsExtends.copy(entity, domain);

        if (!result.isSuccess()) {
            result.setSuccess(false);
            return result;
        }

        entity = setPassword(entity);

        int r = -1;

        try {
            r = sysAdminLoginMapper.updateByPrimaryKeySelective(entity);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.SYSTEM_EXCEPTION);
            return result;
        }

        if (r < 0) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.EDIT_ADMIN_ERROR);
            return result;
        }

        return result;
    }

    public ResultDO remove(Integer id) {

        ResultDO result = new ResultSupport();

        int r = -1;
        try {
            r = sysAdminLoginMapper.deleteByPrimaryKey(id);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.SYSTEM_EXCEPTION);
            return result;
        }

        if (r < 0) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.DELETE_ADMIN_ERROR);
            return result;
        }

        return result;
    }

    public ResultDO get(Integer id) {

        SysAdminLogin entity = null;

        ResultDO result = new ResultSupport();

        try {
            entity = sysAdminLoginMapper.selectByPrimaryKey(id);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.SYSTEM_EXCEPTION);
            return result;
        }
        AdminLoginDO domain = getDomain(entity);

        if (domain == null) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.GET_ADMIN_ERROR);
            return result;
        }
        result.setModel(ResultSupport.FIRST_MODEL_KEY, domain);
        return result;
    }

    public ResultDO query(AdminQueryDO queryDO) {

        ResultDO result = new ResultSupport();

        SysAdminLoginExample example = new SysAdminLoginExample();

        if (StringUtils.isNotEmpty(queryDO.getUsername())) {
            example.createCriteria().andUsernameEqualTo(queryDO.getUsername());
        }

        List<SysAdminLogin> list = null;
        try {
            list = sysAdminLoginMapper.selectByExample(example);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.SYSTEM_EXCEPTION);
            return result;
        }

        List<AdminLoginDO> doList = getDomainList(list);

        if (doList == null) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.SYSTEM_EXCEPTION);
            return result;
        }

        result.setModel(ResultSupport.FIRST_MODEL_KEY, doList);
        return result;
    }

    private AdminLoginDO getDomain(SysAdminLogin entity) {
        if (entity == null) return null;
        AdminLoginDO domain = new AdminLoginDO();
        ResultDO result = roleService.get(entity.getRoleId());
        if (!result.isSuccess()) {
            System.out.println(result.getErrorCode().getMsg());
            return null;
        }
        if (!BeanUtilsExtends.copyProperties(domain, entity)) {
            return null;
        }
        domain.setRole((RoleDO) result.getModel(ResultSupport.FIRST_MODEL_KEY));
        return domain;
    }

    private List<AdminLoginDO> getDomainList(List<SysAdminLogin> list) {
        List<AdminLoginDO> doList = new ArrayList<AdminLoginDO>();
        if (list.size() > 0) {
            for (SysAdminLogin entity : list) {
                AdminLoginDO domain = this.getDomain(entity);
                if (domain != null) {
                    doList.add(domain);
                } else {
                    return null;
                }
            }
        } else {
            return null;
        }
        return doList;
    }

}
