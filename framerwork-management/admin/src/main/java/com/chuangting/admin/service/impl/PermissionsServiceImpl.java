package com.chuangting.admin.service.impl;

import com.chuangting.admin.dao.SysPermissionsMapper;
import com.chuangting.admin.domain.PermissionsDO;
import com.chuangting.admin.entity.SysPermissions;
import com.chuangting.admin.entity.SysPermissionsExample;
import com.chuangting.admin.query.PermissionsQueryDO;
import com.chuangting.admin.service.IPermissionsService;
import com.chuangting.common.enums.ErrorCode;
import com.chuangting.common.result.ResultDO;
import com.chuangting.common.result.ResultSupport;
import com.chuangting.common.util.BeanUtilsExtends;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

<<<<<<< HEAD
@SuppressWarnings("all")
=======
>>>>>>> b0b392cd70b93e1ddfcdfe46b085ba21851d8fbf
@Service
public class PermissionsServiceImpl implements IPermissionsService {

    @Autowired
    private SysPermissionsMapper sysPermissionsMapper;

    public ResultDO add(PermissionsDO domain) {

        SysPermissions entity = new SysPermissions();
        ResultDO result = BeanUtilsExtends.copy(entity, domain);

        if (!result.isSuccess()) {
            result.setSuccess(false);
            return result;
        }

        int r = -1;

        try {
            r = sysPermissionsMapper.insertSelective(entity);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.SYSTEM_EXCEPTION);
            return result;
        }

        if (r < 0) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.ADD_PERMISSIONS_ERROR);
            return result;
        }

        return result;
    }

    public ResultDO edit(PermissionsDO domain) {

        SysPermissions entity = new SysPermissions();
        ResultDO result = BeanUtilsExtends.copy(entity, domain);

        if (!result.isSuccess()) {
            result.setSuccess(false);
            return result;
        }

        int r = -1;

        try {
            r = sysPermissionsMapper.updateByPrimaryKeySelective(entity);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.SYSTEM_EXCEPTION);
            return result;
        }

        if (r < 0) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.EDIT_PERMISSIONS_ERROR);
            return result;
        }

        return result;
    }

    public ResultDO remove(Integer id) {

        ResultDO result = new ResultSupport();

        int r = -1;

        try {
            r = sysPermissionsMapper.deleteByPrimaryKey(id);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.SYSTEM_EXCEPTION);
            return result;
        }

        if (r < 0) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.DELETE_PERMISSIONS_ERROR);
            return result;
        }

        return result;
    }

    public ResultDO get(Integer id) {

        SysPermissions entity = null;

        ResultDO result = new ResultSupport();

        try {
            entity = sysPermissionsMapper.selectByPrimaryKey(id);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.SYSTEM_EXCEPTION);
            return result;
        }
        PermissionsDO domain = getDomain(entity);

        if (domain == null) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.GET_PERMISSIONS_ERROR);
            return result;
        }
        result.setModel(ResultSupport.FIRST_MODEL_KEY, domain);
        return result;
    }

    public ResultDO query(PermissionsQueryDO queryDO) {



        ResultDO result = new ResultSupport();

        SysPermissionsExample example = new SysPermissionsExample();

        if (queryDO.getType() != null) {
            example.createCriteria().andTypeEqualTo(queryDO.getType());
        }

        List<SysPermissions> list = null;
        try {
            list = sysPermissionsMapper.selectByExample(example);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.SYSTEM_EXCEPTION);
            return result;
        }

        List<PermissionsDO> doList = getDomainList(list);

        if (doList == null) {
            result.setSuccess(false);
            result.setErrorCode(ErrorCode.SYSTEM_EXCEPTION);
            return result;
        }

        result.setModel(ResultSupport.FIRST_MODEL_KEY, doList);
        return result;
    }

    private PermissionsDO getDomain(SysPermissions entity) {
        if (entity == null) return null;
        PermissionsDO domain = new PermissionsDO();
        return BeanUtilsExtends.copyProperties(domain, entity) ? domain : null;
    }

    private List<PermissionsDO> getDomainList(List<SysPermissions> list) {
        List<PermissionsDO> doList = new ArrayList<PermissionsDO>();
        if (list.size() > 0) {
            for (SysPermissions entity : list) {
                PermissionsDO domain = this.getDomain(entity);
                if (domain != null) {
                    doList.add(domain);
                } else {
                    return null;
                }
            }
        } else {
            return null;
        }
        return doList;
    }

}
